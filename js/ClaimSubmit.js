const html = `<div class="group-input w-50">
<label for=""
  >Insured Value Amount
  <span style="color: #ffce56">*</span></label
>
<input type="text" class="form-control required" placeholder="$" />
</div>`;

$('input[name="articlesInsured"]').on("change", function () {
  if ($("#articlesInsured-yes").is(":checked")) {
    $(".insuredValueAmount").append(html).hide().show("slow");
  } else if ($("#articlesInsured-no").is(":checked")) {
    $(".insuredValueAmount").children().hide("slow").delay(500, function(){
        $(".insuredValueAmount").empty();
    });
  }
});
$("#addProductClaimed").on("click", function () {
  const html1 = `<tr>
    <td>
      <div class="mb-3">
        <select class="form-select required" name="" id="">
          <option selected>Select</option>
          <option value="">Shortage</option>
          <option value="">Damaged</option>
        </select>
      </div>
    </td>
    <td>
      <div class="mb-3">
        <select class="form-select required" name="" id="">
          <option selected>Select</option>
          <option value="">New</option>
          <option value="">Used</option>
        </select>
      </div>
    </td>
    <td>
      <div class="mb-3">
        <input
          type="text"
          class="form-control required"
          name=""
          id=""
        />
      </div>
    </td>
    <td>
      <div class="mb-3">
        <input type="text" class="form-control" name="" id="" />
      </div>
    </td>
    <td>
      <div class="mb-3">
        <input
          type="text"
          class="form-control required"
          name=""
          id=""
        />
      </div>
    </td>
    <td>
      <div class="mb-3">
        <input
          type="text"
          class="form-control required"
          name=""
          id=""
        />
      </div>
    </td>
    <td>
      <div class="mb-3">
        <input type="text" class="form-control" name="" id="" />
      </div>
    </td>
    <td>
      <div class="mb-3">
        <input
          type="text"
          class="form-control required"
          name=""
          id=""
        />
      </div>
    </td>
    <td>
      <div class="mb-3">
        <input
          type="text"
          class="form-control disabled"
          name=""
          id=""
          readonly
        />
      </div>
    </td>
    <td class='fs-5 deleteProductClaimed' style='cursor: pointer'><i class="bi bi-trash-fill"></i></td>
  </tr>`;
  $("#tbodyProductClaimed").append(html1);
  $("#tbodyProductClaimed").children().last().hide().show("slow")
});
$(document).on("click", ".deleteProductClaimed", function () {
  $(this).closest("tr").hide("slow");
  $(this).closest("tr").delay(500, function(){
    $(this).remove()
  });
});
const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);
const htmlfreightcharges = `
<div class="row">
              <div class="col-2"></div>
              <div class="col-4">
                <h5>OPTION</h5>
                <div class="form-check fs-5 mb-3">
                  <input class="form-check-input" name="" type="checkbox" value="Original Freight Charges"
                    id="Original-Freight-Charges" />
                  <label class="form-check-label" for="Original-Freight-Charges">
                    Original Freight Charges
                  </label>
                </div>
                <div class="form-check fs-5 mb-3">
                  <input class="form-check-input" name="" type="checkbox" value="Return Freight Charges"
                    id="Return-Freight-Charges" />
                  <label class="form-check-label" for="Return-Freight-Charges">
                    Return Freight Charges
                  </label>
                </div>
                <div class="form-check fs-5">
                  <input class="form-check-input" name="" type="checkbox"
                    value="Replacement Freight Charges" id="Replacement-Freight-Charges" />
                  <label class="form-check-label" for="Replacement-Freight-Charges">
                    Replacement Freight Charges
                  </label>
                </div>
              </div>
              <div class="col-2">
                <h5>VALUE</h5>
                <input type="text" class="form-control fs-6 mb-2" disabled value="" id="Original-Freight-Charges-value"
                  placeholder="$" />
                <input type="text" class="form-control fs-6 mb-2" disabled value="" id="Return-Freight-Charges-value"
                  placeholder="$" />
                <input type="text" class="form-control fs-6 " disabled value="" id="Replacement-Freight-Charges-value"
                  placeholder="$" />
              </div>
              <div class="col-4">
                <h5>Primary Ref #</h5>
                <input type="text" class="form-control fs-6 mb-2" disabled style="margin-top: 52px" value=""
                  id="Return-Freight-Charges-primary" />
                <input type="text" class="form-control fs-6" disabled value="" id="Replacement-Freight-Charges-primary" />
              </div>
            </div>
`;
$('input[name="freightCharges"]').on("change", function () {
  if ($("#freightCharges-yes").is(":checked")) {
    $(".form-freight-charges").append(htmlfreightcharges).hide().show("slow");
  } else if ($("#freightCharges-no").is(":checked")) {
    $(".form-freight-charges").hide("slow").empty();
  }
});
$(document).on("change", '#Original-Freight-Charges', function(){
    if($(this).is(":checked")){
      $("#Original-Freight-Charges-value").prop('disabled', false);
    } 
    else{
      $("#Original-Freight-Charges-value").prop('disabled', true);
    }
})
$(document).on("change", '#Return-Freight-Charges', function(){
  if($(this).is(":checked")){
    $("#Return-Freight-Charges-value").prop('disabled', false);
    $("#Return-Freight-Charges-primary").prop('disabled', false);
  } 
  else{
    $("#Return-Freight-Charges-value").prop('disabled', true);
    $("#Return-Freight-Charges-primary").prop('disabled', true);
  }
})
$(document).on("change", '#Replacement-Freight-Charges', function(){
  if($(this).is(":checked")){
    $("#Replacement-Freight-Charges-value").prop('disabled', false);
    $("#Replacement-Freight-Charges-primary").prop('disabled', false);
  } 
  else{
    $("#Replacement-Freight-Charges-value").prop('disabled', true);
    $("#Replacement-Freight-Charges-primary").prop('disabled', true);
  }
})

$(".primary-ref").on("keyup", function(){
    if($(this).val()){
      $(".button-primary-ref").prop("disabled", false);
    }
    else{
      $(".button-primary-ref").prop("disabled", true);
    }
})