const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);
const checkServicesType = (checked) => {
  let checkboxes = document.getElementsByClassName("form-check-input-services");
  for (let i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = checked;
  }
};
const checkSearch = (checked) => {
  let checkboxes = document.getElementsByClassName("form-check-input-search");
  for (let i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = checked;
  }
};
$("#Select-All").click(function () {
  let val = document.getElementById("Select-All").checked;
  checkSearch(val);
});
$(document).ready(function () {
  fnLoadDataTableInstance();
});
function fnLoadDataTableInstance() {
  var dataSource = [
    {
      ref: "<div class='tbl-ref'><a href='./Details.html'>SAN33161125</a><br/><p>PO: N/A</p></div>",
      cusname: "<p>WorldCraft Logistics</p>",
      service: "<p>LTL</p>",
      status: `
        <div style='display: flex; align-items: center'><div style='display: inline-block; width: 20px; height: 20px; background-color: #52b352; margin-right: 5px'></div><p class='d-inline'>Delivere</p></div><p>PRO #: <a href="" style="color: blue">856056751</a></p>
      `,
      pickup: "<p>10/10/2023</p><p>04:57 PM</p>",
      delivery: "<p>10/12/2023</p><p>12:16 PM</p>",
      origin: "<p>Worldcraft Logistics</p><p>Loma Linda, CA 92354</p>",
      destination: "<p>Amazon Warehouse</p><p>Beaumont, CA 92223</p>",
      action: `<div class='btn-action'>
        <button class='btn-small'><i class="fas fa-info"></i></button><button class='btn-small'><i class="fas fa-copy"></i></button><br/>
        <button class='btn-small'><i class="fas fa-undo"></i></button><button class='btn-small'><i class="fas fa-map-marker-alt"></i></button>
      </div>
      `,
    },
    {
      ref: "<div class='tbl-ref'><a href='./Details.html'>SAN33161125</a><br/><p>PO: N/A</p></div>",
      cusname: "<p>WorldCraft Logistics</p>",
      service: "<p>LTL</p>",
      status: `
        <div style='display: flex; align-items: center'><div style='display: inline-block; width: 20px; height: 20px; background-color: #52b352; margin-right: 5px'></div><p style='display: inline'>Delivere</p></div><p>PRO #: <a href="" style="color: blue">856056751</a></p>
      `,
      pickup: "<p>10/10/2023</p><p>04:57 PM</p>",
      delivery: "<p>10/12/2023</p><p>12:16 PM</p>",
      origin: "<p>Worldcraft Logistics</p><p>Loma Linda, CA 92354</p>",
      destination: "<p>Amazon Warehouse</p><p>Beaumont, CA 92223</p>",
      action: `<div class='btn-action'>
      <button class='btn-small'><i class="fas fa-info"></i></button><button class='btn-small'><i class="fas fa-copy"></i></button><br/>
      <button class='btn-small'><i class="fas fa-undo"></i></button><button class='btn-small'><i class="fas fa-map-marker-alt"></i></button>
    </div>
    `,
    },
  ];

  $("#dtExample").DataTable({
    dom: "Bfrtip",
    data: dataSource,
    columns: [
      { data: "ref" },
      { data: "cusname" },
      { data: "service" },
      { data: "status" },
      { data: "pickup" },
      { data: "delivery" },
      { data: "origin" },
      { data: "destination" },
      { data: "action" },
    ],

    paging: true,
    info: true,
    language: {
      emptyTable: "No data available",
    },
  });
}
