// var tags = [];
// var tagsTo = [];
// const tagContainer = $(".tag-container-shipment-from");
// const selectInput = $("#select-services-accessorials-from");

// const tagContainerTo = $(".tag-container-shipment-to");
// const selectInputTo = $("#select-services-accessorials-to");

// const tagShipmentFrom = "tag-shipment-from";
// const tagShipmentTo = "tag-shipment-to";

// function createTag(lable, cls) {
//   const div = document.createElement("div");
//   div.setAttribute("class", `${cls}`);
//   const span = document.createElement("span");
//   span.innerHTML = lable;
//   const btnclose = document.createElement("i");
//   btnclose.setAttribute("class", "bi bi-x-circle");
//   btnclose.setAttribute("data-item", lable);

//   div.appendChild(span);
//   div.appendChild(btnclose);
//   return div;
// }
// function reset(cls) {
//   document.querySelectorAll(`.${cls}`).forEach(function (tag) {
//     tag.parentElement.removeChild(tag);
//   });
// }
// function addTags(list, container, classTagContain) {
//   reset(classTagContain);
//   list
//     .slice()
//     .reverse()
//     .forEach(function (tag) {
//       const input = createTag(tag, classTagContain);
//       container.prepend(input);
//     });
// }

// tagContainer.on("click", (e) => {
//   if (e.target.tagName === "I") {
//     const value = e.target.getAttribute("data-item");
//     const index = tags.indexOf(value);
//     tags = [...tags.slice(0, index), ...tags.slice(index + 1)];
//     addTags(tags, tagContainer, tagShipmentFrom);
//     $("#services-accessorials-from").val(tags.join(";"));
//   }
// });

// selectInput.on("change", function () {
//   if (selectInput.val() != "") {
//     if (selectInput.val() === "Select All") {
//       tags = [];
//       tags.push(selectInput.val());
//     } else {
//       tags.push(selectInput.val());
//     }
//   }
//   addTags(tags, tagContainer, tagShipmentFrom);
//   $("#services-accessorials-from").val(tags.join(";"));
// });

// tagContainerTo.on("click", (e) => {
//   if (e.target.tagName === "I") {
//     const value = e.target.getAttribute("data-item");
//     const index = tagsTo.indexOf(value);
//     tagsTo = [...tagsTo.slice(0, index), ...tagsTo.slice(index + 1)];
//     addTags(tagsTo, tagContainerTo, tagShipmentTo);
//     $("#services-accessorials-to").val(tagsTo.join(";"));
//   }
// });

// selectInputTo.on("change", function () {
//   tagsTo.push(selectInputTo.val());
//   addTags(tagsTo, tagContainerTo, tagShipmentTo);
//   $("#services-accessorials-to").val(tagsTo.join(";"));
// });

$(document).ready(function () {
  fnLoadDataTableInstance();
});
function fnLoadDataTableInstance() {
  var dataSource = [
    {
      staCust: "<p>33-San Diego CA WorldCraft Logistics</p>",
      customerRef: "<p>WorldCraft Logistics</p>",
      insured: "<p>N</p>",
      claim: `
      <p><a href="" style="color: blue">T2021003555</a></p>
        `,
      primaryRef: "<p>SAN33126232</p>",
      carrier: "<p>FEDEX</p><p>Pro: 5392387361</p>",
      amount: "<p>$1,640.39</p>",
      status: `
      <div style='display: flex; align-items: center'><div style='display: inline-block; width: 15px; height: 15px; background-color: #52b352; margin-right: 5px'></div><p class='d-inline'>Paid</p></div>
    `,
      submitDate: `<p>07/17/2021</p>`,
    },
    {
      staCust: "<p>33-San Diego CA WorldCraft Logistics</p>",
      customerRef: "<p>WorldCraft Logistics</p>",
      insured: "<p>N</p>",
      claim: `
      <p><a href="" style="color: blue">T2021003555</a></p>
        `,
      primaryRef: "<p>SAN33131380</p>",
      carrier: "<p>UPS Freight</p><p>Pro: 297163580</p>",
      amount: "<p>$22,353.48</p>",
      status: `
      <div style='display: flex; align-items: center'><div style='display: inline-block; width: 15px; height: 15px; background-color: blue; margin-right: 5px'></div><p class='d-inline'>Open</p></div>
    `,
      submitDate: `<p>10/04/2021</p>`,
    },
  ];

  $("#dtExample").DataTable({
    dom: "Bfrtip",
    data: dataSource,
    columns: [
      { data: "staCust" },
      { data: "customerRef" },
      { data: "insured" },
      { data: "claim" },
      { data: "primaryRef" },
      { data: "carrier" },
      { data: "amount" },
      { data: "status" },
      { data: "submitDate" },
    ],

    paging: true,
    info: true,
    language: {
      emptyTable: "No data available",
    },
  });
}

$(document).ready(function () {
  $("#myTable").DataTable();
});
$.ajax({
  url: "/list-save-address",
  method: "post",
  data: {},
  async: false,
  success: function (respone){
      console.log(respone);
      $(respone).each(function(index, item){
          console.log(item.name);
          var html = `<li>${item.name};${item.address};${item.address2};${item.zip ? item.zip : 0};${item.city};${item.country};${item.state};${item.contact_Name};${item.contact_Email};${item.contact_Phone};</li>`
          $(".list-data-shipping-from").append(html);
          $(".list-data-shipping-to").append(html);
      })
      
  }
})