const checkSearch = (checked) => {
  let checkboxes = document.getElementsByClassName("form-check-input-search");
  for (let i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = checked;
  }
};
$("#Select-All").click(function () {
  let val = document.getElementById("Select-All").checked;
  checkSearch(val);
});
$(document).ready(function () {
  $(".quotelist1").parent().parent().parent().next().hide();
  $(".quotelist1").click(function () {
    $(this).parent().parent().parent().next().toggle("slow");
  });
  $(".quotelist2").parent().parent().parent().next().hide();
  $(".quotelist2").click(function () {
    $(this).parent().parent().parent().next().toggle("slow");
  });
});