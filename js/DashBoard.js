const validateEmail = (email) => {
    return email.match(
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

const validatePhoneNumber = (phone) => {
    return phone.match(
        /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
    );
};

var check = true;
$(".icon-nav").click(function() {
    $("nav").toggleClass("nav-responsive")
    if(check){
        $("body").css("grid-template-columns", "20rem 1fr");
        check = !check;
    }
    else{
        $("body").css("grid-template-columns", "100px 1fr");
        check = !check;
    }


})
// $("nav div").hover(function(){
//     $(this).toggleClass("d-none")
// })
const ctx = document.getElementById('myChart');

    new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: ['Appointment Acheduled', 'Relivered', 'Rejected'],
        datasets: [{
          label: '',
          data: [3, 4, 1],
          borderWidth: 1
        }]
      },
      options: {
        plugins: {
        legend: {
            display: false
        },
        },
    }
});
///////////////////////////////////////////////////////////////////////////
var tags =[];
var tagsTo = [];
const tagContainer  = $(".tag-container-shipment-from");
const selectInput = $("#select-services-accessorials-from");

const tagContainerTo = $(".tag-container-shipment-to");
const selectInputTo = $("#select-services-accessorials-to");

const tagShipmentFrom = 'tag-shipment-from';
const tagShipmentTo = 'tag-shipment-to';

function createTag(lable, cls) {
    const div = document.createElement('div')
        div.setAttribute('class', `${cls}`)
    const span = document.createElement('span')
        span.innerHTML = lable;
    const btnclose = document.createElement('i')
        btnclose.setAttribute('class', 'bi bi-x-circle');
        btnclose.setAttribute('data-item', lable);

    div.appendChild(span)
    div.appendChild(btnclose)
    return div;
}
function reset (cls){
    document.querySelectorAll(`.${cls}`).forEach(function(tag){
        tag.parentElement.removeChild(tag);
    })
}
function addTags(list, container, classTagContain){
    reset(classTagContain)
    list.slice().reverse().forEach(function(tag) {
        const input = createTag(tag, classTagContain);
        container.prepend(input);
    })
}

tagContainer.on('click', (e) =>{
    if(e.target.tagName === 'I'){
        const value = e.target.getAttribute('data-item');
        console.log(value);
        const index = tags.indexOf(value)
        tags = [...tags.slice(0, index), ...tags.slice(index + 1)]
        addTags(tags, tagContainer, tagShipmentFrom);
        $("#services-accessorials-from").val(tags.join(";"));
    }
})

selectInput.on('change', function(){
    tags.push(selectInput.val())
    addTags(tags, tagContainer, tagShipmentFrom);
    $("#services-accessorials-from").val(tags.join(";"));
})

tagContainerTo.on('click', (e) =>{
    if(e.target.tagName === 'I'){
        const value = e.target.getAttribute('data-item');
        console.log(value);
        const index = tagsTo.indexOf(value)
        tagsTo = [...tagsTo.slice(0, index), ...tagsTo.slice(index + 1)]
        addTags(tagsTo, tagContainerTo, tagShipmentTo);
        $("#services-accessorials-to").val(tagsTo.join(";"));
    }
})

selectInputTo.on('change', function(){
    tagsTo.push(selectInputTo.val())
    addTags(tagsTo, tagContainerTo, tagShipmentTo);
    $("#services-accessorials-to").val(tagsTo.join(";"));
})
///////////////////////////////////////////////////////////////////////////
// -- Pick up date --//

$(document).ready(function () {
    const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]');
    const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl));
});
$(document).ready( function() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
   $('#date-pickup-date').val(today);
   $('.date-now').val(today)
});
function getHour24(timeString)
{
    time = null;
    var matches = timeString.match(/^(\d{1,2}):00 (\w{2})/);
    console.log(timeString)
    if (matches != null && matches.length == 3)
    {
        time = parseInt(matches[1]);
        if (matches[2] == 'PM')
        {
            time += 12;
        }
    }
    return time;
}

$("#pick-up-date-close").on('change', function(){
    const timeReady = $("#pick-up-date-ready").val();
    const timeClose = $("#pick-up-date-close").val();
    if(timeReady === ''){
        $("#pick-up-date-ready").parent().addClass("border-danger");
        Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'Please select a Pickup Date Ready time that is before the Pickup Date Close time.',
          })
    }

    var timeReadyVal = new Date("November 10, 2023 " + timeReady);
    timeReadyVal = timeReadyVal.getTime();

    var timeCloseVal = new Date("November 10, 2023 " + timeClose);
    timeCloseVal = timeCloseVal.getTime();

    if(timeCloseVal < timeReadyVal){
        $("#pick-up-date-close").parent().addClass("border-danger");
        $("#pick-up-date-ready").parent().removeClass("border-danger");
        Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'Please select a Pickup Date Close time that is after the Pickup Date Ready time.',
          })
    }
    else{
        $("#pick-up-date-ready").parent().removeClass("border-danger");
        $("#pick-up-date-close").parent().removeClass("border-danger");
    }
})
///////////////////////////////////////////////////////////////////////////
$("#shipment-value").on('change', function(){
    if($("#shipment-value").val() != "" && $.isNumeric($("#shipment-value").val())){
        var a = $("#shipment-value").val()
        $("#shipment-value").val(`$${a}.00`);
        $("#button-term-and-conditions").removeClass("d-none")
    }
    else{
        $("#shipment-value").val("");
    }
})

$('input[name="ShipmentProtection"]').on('change', function(){
    console.log(123)
    if($("#Shipment-protection-yes").is(":checked")){
        $("#shipment-value").addClass("required");
        $("#shipment-value").attr("required", true);
    }
    if($("#Shipment-protection-no").is(":checked")){
        $("#shipment-value").removeClass("required");
        $("#shipment-value").attr("required", false);
    }
})
///////////////////////////////////////////////////////////////////////////
$(".reference-number-select").on("change", function(){
    if($(".reference-number-select").val() != ""){
        $(".input-reference").addClass("required")
        $(".input-reference").attr("required", true);
        $(".reference-number-select").addClass("required")
    }
    else{
        $(".input-reference").removeClass("required")
        $(".input-reference").attr("required", false);
    }
})
var state = true;
$("#btn-add-edit").on('click', function(){
    if(state){
        $("#btn-add-edit").text("Save/close");
        $("#btn-add-edit").css("color", "#ffce56")
        state = !state}
    else if(!state){
        $("#btn-add-edit").text("Add/Edit");
        $("#btn-add-edit").css("color", "#3e5bb1")
        state = !state}
})
$(".add-additional").on('click', function(){
    const html = `<div class="row">
    <div class="col-5">
        <select name="" class="reference-number-select form-select">
            <option selected>Select...</option>
            <option value="BOL Number">BOL Number</option>
            <option value="Buyer">Buyer</option>
            <option value="Carrier Quote Number">Carrier Quote Number</option>
            <option value="Cons Ref">Cons Ref</option>
            <option value="CustInvRef">CustInvRef</option>
            <option value="Delivery Appt Number">Delivery Appt Number</option>
            <option value="GL Code">GL Code</option>
            <option value="Job Name">Job Name</option>
            <option value="Job Number">Job Number</option>
            <option value="Manifest Nbr">Manifest Nbr</option>
            <option value="Order Number">Order Number</option>
            <option value="Pickup Appt Nbr">Pickup Appt Nbr</option>
            <option value="Pickup Number">Pickup Number</option>
            <option value="PO Number">PO Number</option>
            <option value="Pro Number">Pro Number</option>
            <option value="Product Code">Product Code</option>
            <option value="Project Number">Project Number</option>
            <option value="QuoteNumber">QuoteNumber</option>
            <option value="Release Nbr">Release Nbr</option>
            <option value="RMA Number">RMA Number</option>
            <option value="Sales Order">Sales Order</option>
            <option value="Service Level">Service Level</option>
            <option value="Ship Ref">Ship Ref</option>
            <option value="WorkOrderNbr">WorkOrderNbr</option>
        </select>
    </div>
    <div class="col-5">
        <div class="group-input">
            <input class="form-control input-reference" type="text" placeholder="Reference #...">
        </div>
    </div>
    <div class="col-2 d-flex justify-content-center align-items-center">
        <button class="btn-close btn-remove-reference-number"></button>
    </div>
</div>`
    $(".container-add-reference-number").append( html)
})

$(document).on('click', '.btn-remove-reference-number', function(){
    $(this).closest('.row').remove();
})
///////////////////////////////////////////////////////////////////////////
// FREIGHT DESCRIPTION
$(".btn-freight-des-additional").on('click', function(){
    var length = $(".freight-description-item").length
    const html = `<div class="row py-2 border-bottom g-0 freight-description-item">
    <div class="col-5 ps-1">
        <div class="row align-items-center">
            <div class="col-2">
                <div class="group-input">
                    <input class="form-check-input" type="checkbox">
                    <label for="">Save item</label>
                </div>
            </div>
            <div class="col-3">
                <div class="group-input">
                    <div class="input-icon ">
                        <input class="form-control required" type="text" placeholder="Class or saved item" required>
                        <i class="bi bi-search icon me-3" style="cursor: pointer; background-color: white;"></i>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <i class="bi bi-x-diamond-fill" style="cursor: pointer;color: #3e5bb1;">Clear</i>
            </div>
            <div class="col-3">
                <div class="group-input">
                    <input class="form-control required" type="text" placeholder="Description" required>
                </div>
            </div>
            <div class="col-2">
                <div class="group-input d-inline">
                    <input class="form-check-input hazmat-detail-checked" type="checkbox" data-bs-toggle="modal" data-bs-target="#haz-freight-description-model-${length}">
                    <label for="">Haz</label>
                </div>
                <i class="bi bi-pencil-fill ps-2 d-none" style="cursor: pointer;" data-bs-toggle="modal" data-bs-target="#haz-freight-description-model-${length}"></i>
                <div class="modal fade" id="haz-freight-description-model-${length}" data-bs-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content">
                        <div class="modal-header p-0">
                            <h2 class="accordion-button d-inline-block py-2 ps-2">Hazmat Details
                                <button  type="button" class="button-outline border-0 float-end me-3 btn-close-modal-haz" data-bs-dismiss="modal" aria-label="Close">Save & close</button>
                            </h2>
                        </div>
                        <div class="modal-body">
                            <div class="row my-3">
                                <div class="col-6">
                                    <div class="input-group">
                                        <input class="form-control required" type="number" placeholder="UN number" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group">
                                        <input class="form-control" type="number" placeholder="CCN number">
                                    </div>
                                </div>

                                <div class="col-6 mt-3">
                                    <div class="input-group">
                                        <select class="form-select required" required>
                                            <option value="">Select Hazmat packaging group...</option>
                                            <option value="I">I</option>
                                            <option value="II">II</option>
                                            <option value="III">III</option>
                                            <option value="NA">NA</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6">
                                    <div class="input-group mt-3">
                                        <select class="form-select required" required>
                                            <option value="">Select Hazmat class...</option>
                                            <option value="1">1</option>
                                            <option value="1.1">1.1</option>
                                            <option value="1.2">1.2</option>
                                            <option value="1.3">1.3</option>
                                            <option value="1.4">1.4</option>
                                            <option value="1.4G">1.4G</option>
                                            <option value="1.4S">1.4S</option>
                                            <option value="1.5">1.5</option>
                                            <option value="1.6">1.6</option>
                                            <option value="2">2</option>
                                            <option value="2.1">2.1</option>
                                            <option value="2.2">2.2</option>
                                            <option value="2.3">2.3</option>
                                            <option value="3">3</option>
                                            <option value="3(6.1)">3(6.1)</option>
                                            <option value="3(6.1)(8)">3(6.1)(8)</option>
                                            <option value="3(8)">3(8)</option>
                                            <option value="4">4</option>
                                            <option value="4.1">4.1</option>
                                            <option value="4.2">4.2</option>
                                            <option value="4.3">4.3</option>
                                            <option value="5">5</option>
                                            <option value="5.1">5.1</option>
                                            <option value="5.1(8)">5.1(8)</option>
                                            <option value="5.1(8)(6.1)">5.1(8)(6.1)</option>
                                            <option value="5.2">5.2</option>
                                            <option value="5.2(8)">5.2(8)</option>
                                            <option value="6">6</option>
                                            <option value="6.1">6.1</option>
                                            <option value="6.1(3)(8)">6.1(3)(8)</option>
                                            <option value="6.1(8)">6.1(8)</option>
                                            <option value="6.2">6.2</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="8(3)">8(3)</option>
                                            <option value="8(5.1)">8(5.1)</option>
                                            <option value="8(6.1)">8(6.1)</option>
                                            <option value="9">9</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-6 mt-3">
                                    <div class="input-group">
                                        <input class="form-control required" type="text" placeholder="Emergency response contact name..." required>
                                    </div>
                                </div>
                                <div class="col-6 mt-3">
                                    <div class="input-group">
                                        <input class="form-control required" type="text" placeholder="Emergency response phone number..." required>
                                    </div>
                                </div>
                            </div>
                        </div>

                      </div>
                    </div>
                </div>
            </div>


              <!-- Modal -->

        </div>
    </div>
    <div class="col-7">
        <div class="row g-1">
            <div class="col-4">
                <div class="row g-1">
                    <div class="col-4">
                        <div class="group-input">
                            <input class="form-control" type="text" placeholder="NMFC">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="group-input">
                            <input class="form-control required" type="text" placeholder="QTY" required>
                        </div>
                    </div>
                    <div class="col-5">
                        <select class="form-select" name="" id="">
                            <option value="Skids">Skids</option>
                            <option value="Bags">Bags</option>
                            <option value="Bundles">Bundles</option>
                            <option value="Boxes">Boxes</option>
                            <option value="Cabinets">Cabinets</option>
                            <option value="Cans">Cans</option>
                            <option value="Cases">Cases</option>
                            <option value="Crates">Crates</option>
                            <option value="Cartons">Cartons</option>
                            <option value="Cylinders">Cylinders</option>
                            <option value="Drums">Drums</option>
                            <option value="Pails">Pails</option>
                            <option value="Pieces">Pieces</option>
                            <option value="Pallets">Pallets</option>
                            <option value="Flat Racks">Flat Racks</option>
                            <option value="Reels">Reels</option>
                            <option value="Rolls">Rolls</option>
                            <option value="Slip Sheets">Slip Sheets</option>
                            <option value="Stacks">Stacks</option>
                            <option value="Totes">Totes</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row g-1">
                    <div class="col-4">
                        <div class="group-input">
                            <input class="form-control required" type="text" placeholder="Weight" required>
                        </div>
                    </div>
                    <div class="col-5">
                        <select class="form-select" name="" id="">
                            <option value="LBS">LBS</option>
                            <option value="KILOS">KILOS</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <div class="group-input">
                            <input class="form-control required" type="text" placeholder="L" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row g-1">
                    <div class="col-4">
                        <div class="group-input">
                            <input class="form-control required" type="text" placeholder="W" required>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="group-input">
                            <input class="form-control required" type="text" placeholder="H" required>
                        </div>
                    </div>
                    <div class="col-5">
                        <select class="form-select" name="" id="">
                            <option value="IN">IN</option>
                            <option value="FT">FT</option>
                            <option value="CM">CM</option>
                            <option value="M">M</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
    $(".container-freight-description").append(html);
})
//btn-close-modal-haz / modal fade
$(document).on('click',".hazmat-detail-checked", function(){
    if($(this).is(":checked")){
        $(this).parent().next().removeClass("d-none")
        $(this).removeAttr("data-bs-target");
        $(this).parent().parent().find("input, select").each(function(index){
            if($(this).hasClass("required")){
                $(this).prop("required", true)
            }
        })
        var eleInputs = $(this).parent().parent();
        $(this).parent().parent().find(".btn-close-modal-haz").removeAttr("data-bs-dismiss")
        $(this).parent().parent().find(".btn-close-modal-haz").on('click', function(e){
            if(validateInputs(eleInputs)){
                $(this).addClass("border-0");
               $(this).attr("data-bs-dismiss", "modal")
            }
            else{
                $(this).removeClass("border-0");
                $(this).css("border", "3px solid #e70f0f")
            }
        })
    }
    else{
        $(this).parent().parent().find("input, select").each(function(index){
            if($(this).hasClass("required")){
                $(this).prop("required", false)
            }
        })
        $(this).parent().next().addClass("d-none")
    }
})

$(".search-origin-input-container").on('click', function(e){
    if(e.target.closest("input") != null){
        $(".list-data-shipping").css("height", "200px");
        $(".search-origin-input-container input").val("").trigger("keyup")
    }

    else if(e.target.closest("li") != null){
        $(".search-origin-input-container input").val(e.target.closest("li").textContent)
        $(".list-data-shipping").css("height", "0px");
        var infors = e.target.closest("li").textContent.split(";");
        $(infors).each(function(index, infor){
            $(".input-shipment-from-container").find(`[fill-data-${index + 1}]`).val(infor);
        })
        var text = `https://www.google.com/maps/place/`;
        text += $(".input-shipment-from-container").find("[fill-data-2]").val().split(' ').join("+") + '+'
                + $(".input-shipment-from-container").find("[fill-data-5]").val().replace(" ", "+") + '+'
                + $(".input-shipment-from-container").find("[fill-data-7]").val().replace(" ", "+") + '+'
                + $(".input-shipment-from-container").find("[fill-data-4]").val().replace(" ", "+");
        console.log(text);
        $(".input-shipment-from-container").find(".view-on-map").attr("href", text)
    }
    else{
        $(".list-data-shipping").css("height", "0px");
    }
    $(".input-search-shipping-from").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".list-data-shipping-from li").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            var keyWord = $(".input-search-shipping-from").val();
            var replaceD = "<span class='highlight'>" + keyWord + "</span>";
            var text = $(this).text();
            text = text.replace(keyWord, replaceD);
            $(this).html(text);
        });
    });
})

$(".search-delivery-input-container").on('click', function(e){
    if(e.target.closest("input") != null){
        $(".list-data-shipping-to").css("height", "200px");
        $(".input-search-shipping-to").val("").trigger("keyup")
    }

    else if(e.target.closest("li") != null){
        $(".search-delivery-input-container input").val(e.target.closest("li").textContent)
        $(".list-data-shipping-to").css("height", "0px");

        var infors = e.target.closest("li").textContent.split(";");
        $(infors).each(function(index, infor){
            $(".input-shipment-to-container").find(`[fill-data-${index + 1}]`).val(infor);
        })
    }
    else{
        $(".list-data-shipping-to").css("height", "0px");
    }
    $(".input-search-shipping-to").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".list-data-shipping-to li").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            var keyWord = $(".input-search-shipping-to").val();
            var replaceD = "<span class='highlight'>" + keyWord + "</span>";
            var text = $(this).text();
            text = text.replace(keyWord, replaceD);
            $(this).html(text);
        });
    });
    $(".input-search-shipping-to").on("focus", function(){
        $(this).select()
    })
})

$("body").on("click", function(e){
   if($(e.target).closest(".search-origin-input-container input").length === 0){
    $(".list-data-shipping-from").css("height", "0px");
   }
   if($(e.target).closest(".search-delivery-input-container input").length === 0){
    $(".list-data-shipping-to").css("height", "0px");
   }
})

$(".input-shipment-from-container").find("[fill-data-2]").on("change", function(){
    var text = `https://www.google.com/maps/place/`;
    text += $(".input-shipment-from-container").find("[fill-data-2]").val().split(' ').join("+") + '+'
            + $(".input-shipment-from-container").find("[fill-data-5]").val().replace(" ", "+") + '+'
            + $(".input-shipment-from-container").find("[fill-data-7]").val().replace(" ", "+") + '+'
            + $(".input-shipment-from-container").find("[fill-data-4]").val().replace(" ", "+");
    console.log(text);
    $(this).find(".view-on-map").attr("href", "")
})


$('form[name=form-add-shipment-LTL]').on("click",'button[type=submit]', function(e){
    e.preventDefault();
    var eleInput = $('form[name=form-add-shipment-LTL]');
    if(validateInputs(eleInput)){
        $('form[name=form-add-shipment-LTL]').submit();
    }
    else{
        Swal.fire({
            icon: 'warning',
            title: 'Warning!',
            text: 'Please fill all fields outlined in orange are required to go next step',
          })
    }
})

$('.clear-address').on('click', function(){
    $(this).parent().parent().find("input").each(function(){
        $(this).val("")
    })
})
////////////////////////////////////////////////////////////////////////////////////////////////////
// ADD SHIPMENT TRUCKLOAD
////////////////////////////////////////////////////////////////////////////////////////////////////
$(document).on("click",".button-add-date-range-origin", function(){
    var html = `<div class="row g-2 mt-3 add-new">
    <div class="col-3">
        <div class="group-input">
            <label for="">DROPOFF DATE(LASTEST) <span style="color: #ffce56 ;">*</span></label>
            <input type="date" class="form-control required" required>
        </div>
    </div>
    <div class="col-3">
        <div class="group-input">
            <label for="">READY <span style="color: #ffce56 ;">*</span></label>
            <input type="time" class="form-control required" required>
        </div>
    </div>
    <div class="col-1 text-center align-self-center">
        <div class="group-input">
            <label for="">TO</label>
        </div>
    </div>
    <div class="col-3">
        <div class="group-input">
            <label for="">CLOSE <span style="color: #ffce56 ;">*</span></label>
            <input type="time" class="form-control required" required>
        </div>
    </div>
    <div class="col-2 align-self-end">
        <button type="button" class="button-primary float-end button-remove-date-range-origin">Remove date range</button>
    </div>
    </div>`
    $(".container-pickup-date-origin").append(html);
    $(".add-new").hide().show('slow')
    $(this).slideUp();
})
$(document).on("click",".button-remove-date-range-origin", function(){
    $(this).closest(".row").slideUp();
    setTimeout(() => {
        $(this).closest(".row").remove();
    }, 500)
    $(".button-add-date-range-origin").slideDown()
})

$(document).on("click",".button-add-date-range-destination", function(){
    var html = `<div class="row g-2 add-new-destination">
    <div class="col-3">
        <div class="group-input">
            <label for="">DROPOFF DATE(LASTEST) <span style="color: #ffce56 ;">*</span></label>
            <input type="date" class="form-control required" required>
        </div>
    </div>
    <div class="col-3">
        <div class="group-input">
            <label for="">READY <span style="color: #ffce56 ;">*</span></label>
            <input type="time" class="form-control required" required>
        </div>
    </div>
    <div class="col-1 text-center align-self-center">
        <div class="group-input">
            <label for="">TO</label>
        </div>
    </div>
    <div class="col-3">
        <div class="group-input">
            <label for="">CLOSE <span style="color: #ffce56 ;">*</span></label>
            <input type="time" class="form-control required" required>
        </div>
    </div>
    <div class="col-2 align-self-end">
        <button type="button" class="button-primary float-end button-remove-date-range-destination">Remove date range</button>
    </div>
</div>`
    $(".container-pickup-date-destination").append(html);
    console.log($(".add-new"));
    $(".add-new-destination").hide().show('slow')
    $(this).slideUp();
})
$(document).on("click",".button-remove-date-range-destination", function(){
    $(this).closest(".row").slideUp();
    setTimeout(() => {
        $(this).closest(".row").remove();
    }, 500)
    $(".button-add-date-range-destination").slideDown()
})

$('input[name="hazardous-materials"]').on("change", function(){
    const html = `<div class="row g-2">
    <!--PROPER SHIPMENT NAME-->
    <div class="col-12">
        <div class="group-input">
            <label for="">PROPER SHIPMENT NAME<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
    <!--UN NUMBER-->
    <div class="col-6">
        <div class="group-input">
            <label for="">UN NUMBER<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
    <!--CCN NUMBER-->
    <div class="col-6">
        <div class="group-input">
            <label for="">CCN NUMBER<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
    <!--HAZMAT PACKAGING GROUP-->
    <div class="col-6">
        <div class="group-input">
            <label for="">HAZMAT PACKAGING GROUP<span style="color: #ffce56;">*</span></label>
            <select class="form-select required" name="" id="" required>
                <option value="">Select Group</option>
                <option value="I">I</option>
                <option value="II">II</option>
                <option value="III">III</option>
                <option value="N/A">N/A</option>
            </select>
        </div>
    </div>
    <!---HAZMAT CLASS-->
    <div class="col-6">
        <div class="group-input">
            <label for="">HAZMAT CLASS<span style="color: #ffce56;">*</span></label>
            <select class="form-select required" required>
                <option value="">Select Hazmat class...</option>
                <option value="1">1</option>
                <option value="1.1">1.1</option>
                <option value="1.2">1.2</option>
                <option value="1.3">1.3</option>
                <option value="1.4">1.4</option>
                <option value="1.4G">1.4G</option>
                <option value="1.4S">1.4S</option>
                <option value="1.5">1.5</option>
                <option value="1.6">1.6</option>
                <option value="2">2</option>
                <option value="2.1">2.1</option>
                <option value="2.2">2.2</option>
                <option value="2.3">2.3</option>
                <option value="3">3</option>
                <option value="3(6.1)">3(6.1)</option>
                <option value="3(6.1)(8)">3(6.1)(8)</option>
                <option value="3(8)">3(8)</option>
                <option value="4">4</option>
                <option value="4.1">4.1</option>
                <option value="4.2">4.2</option>
                <option value="4.3">4.3</option>
                <option value="5">5</option>
                <option value="5.1">5.1</option>
                <option value="5.1(8)">5.1(8)</option>
                <option value="5.1(8)(6.1)">5.1(8)(6.1)</option>
                <option value="5.2">5.2</option>
                <option value="5.2(8)">5.2(8)</option>
                <option value="6">6</option>
                <option value="6.1">6.1</option>
                <option value="6.1(3)(8)">6.1(3)(8)</option>
                <option value="6.1(8)">6.1(8)</option>
                <option value="6.2">6.2</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="8(3)">8(3)</option>
                <option value="8(5.1)">8(5.1)</option>
                <option value="8(6.1)">8(6.1)</option>
                <option value="9">9</option>
            </select>
        </div>
    </div>
    <!--EMERGENCY RESPONSE CONTACT NAME *-->
    <div class="col-6">
        <div class="group-input">
            <label for="">EMERGENCY RESPONSE CONTACT NAME<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
    <div class="col-6">
        <div class="group-input">
            <label for="">EMERGENCY RESPONSE PHONE NUMBER<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
</div>
`
    if($("#hazardous-materials-yes").is(":checked")){
        $(".hazardous-matetials-container").append(html).hide().show("slow");
    }
    else if($("#hazardous-materials-no").is(":checked")){
        $(".hazardous-matetials-container").hide("slow");
        $(".hazardous-matetials-container").empty();
    }
})

// add additional item shipment truck load
$(".truck-load-add-additional-item").on("click", function(){
    console.log('add')
    var length = $(".truck-load-add-remove-addition-container").find(".add-container").length;
    const html = `<div class="add-container my-3 clearfix">
    <p>Select a Saved Item</p>
    <div class="row pb-3 border-bottom">
        <div class="col-6">
            <select name="" id="" class="form-select">
                <option value="">Select item</option>
                <option value="">khanhtien</option>
                <option value="">khanhtien</option>
            </select>
        </div>
    </div>
    <p class="mt-3">or Enter New Item Information</p>
    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="col-4">
                    <div class="group-input">
                        <label for="">QUANTITY <span style="color: #ffce56;">*</span></label>
                        <input class="form-control" type="number" name="" id="">
                    </div>
                </div>
                <div class="col-8">
                    <label for=""></label>
                    <select class="form-select" name="" id="">
                        <option value="Bags">Bags</option>
                        <option value="Bundles">Bundles</option>
                        <option value="Boxes">Boxes</option>
                        <option value="Cabinets">Cabinets</option>
                        <option value="Cans">Cans</option>
                        <option value="Cases">Cases</option>
                        <option value="Crates">Crates</option>
                        <option value="Cartons">Cartons</option>
                        <option value="Cylinders">Cylinders</option>
                        <option value="Drums">Drums</option>
                        <option value="Pails">Pails</option>
                        <option value="Pieces">Pieces</option>
                        <option value="Pallets">Pallets</option>
                        <option value="Flat Racks">Flat Racks</option>
                        <option value="Reels">Reels</option>
                        <option value="Rolls">Rolls</option>
                        <option value="Skids">Skids</option>
                        <option value="Slip Sheets">Slip Sheets</option>
                        <option value="Stacks">Stacks</option>
                        <option value="Totes">Totes</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-8">
                    <div class="group-input">
                        <label for="">CLASSIFICATION</label>
                        <select class="form-select" name="" id="">
                            <option value="50">50</option>
                            <option value="55">55</option>
                            <option value="60">60</option>
                            <option value="65">65</option>
                            <option value="70">70</option>
                            <option value="77.5">77.5</option>
                            <option value="85">85</option>
                            <option value="92.5">92.5</option>
                            <option value="100">100</option>
                            <option value="110">110</option>
                            <option value="125">125</option>
                            <option value="150">150</option>
                            <option value="175">175</option>
                            <option value="200">200</option>
                            <option value="250">250</option>
                            <option value="300">300</option>
                            <option value="400">400</option>
                            <option value="500">500</option>
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="group-input">
                        <label for="">NMFC</label>
                        <input class="form-control" type="text" name="" id="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-4">
            <div class="row">
                <div class="col-4">
                    <div class="group-input">
                        <label for="">TOTAL WEIGHT<span style="color: #ffce56;">*</span></label>
                        <input type="number" class="form-control required" required>
                    </div>
                </div>
                <div class="col-4">
                    <div class="group-input">
                        <label for=""></label>
                        <select name="" id="" class="form-select">
                            <option value="LBS">LBS</option>
                            <option value="KILOS">KILOS</option>
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="group-input">
                        <label for="">DIMENSIONS</label>
                        <input type="number" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="row">
                <div class="col-4">
                    <div class="group-input">
                        <label for=""></label>
                        <input type="number" class="form-control" placeholder="WIDTH">
                    </div>
                </div>
                <div class="col-4">
                    <div class="group-input">
                        <label for=""></label>
                        <input type="number" class="form-control" placeholder="HEIGHT">
                    </div>
                </div>
                <div class="col-4">
                    <div class="group-input">
                        <label for=""></label>
                        <select name="" id="" class="form-select">
                            <option value="IN">IN</option>
                            <option value="CM">CM</option>
                            <option value="FT">FT</option>
                            <option value="METER">METER</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="row">
                <div class="col-6">
                    <div class="group-input">
                        <label for="">SHIPMENT VALUE</label>
                        <input id="shipment-value" type="text" class="form-control">
                    </div>
                </div>

                <div class="col-6">
                    <div class="group-input">
                        <label for="">INCLUDE PROTECTION? <span style="color: #ffce56;">*</span></label>
                        <div class="check-box-radio-group">
                            <input class="check-box-input d-none" type="radio" id="Shipment-protection-yes" value="True" name="ShipmentProtection">
                            <label class="check-box-label" for="Shipment-protection-yes">
                                <span class="circle-check-box mt-1"></span>
                                Yes
                            </label>
                        </div>

                        <div class="check-box-radio-group">
                            <input class="check-box-input d-none" type="radio" id="Shipment-protection-no" value="False" name="ShipmentProtection">
                            <label class="check-box-label" for="Shipment-protection-no">
                                <span class="circle-check-box mt-1"></span>
                                No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 mt-3">
            <div class="group-input">
                <label for="">ITEM DESCRIPTION <span style="color: #ffce56;">*</span></label>
                <input type="text" class="form-control required" required>
            </div>
        </div>
        <div class="col-12 mt-3 expand-yes">
            <div class="group-input">
                <label for="" class="d-block mb-3">HAZARDOUS MATERIALS<span style="color: #ffce56;">*</span></label>
                <div class="check-box-radio-group">
                    <input class="check-box-input d-none input-check-hazardous-materials" type="radio" id="hazardous-materials-add-yes-${length}" value="True" name="hazardous-materials-get-quote-${length}">
                    <label class="check-box-label" for="hazardous-materials-add-yes-${length}">
                        <span class="circle-check-box mt-1"></span>
                        Yes
                    </label>
                </div>

                <div class="check-box-radio-group">
                    <input class="check-box-input d-none input-check-hazardous-materials" type="radio" id="hazardous-materials-add-no-${length}" value="False" name="hazardous-materials-get-quote-${length}">
                    <label class="check-box-label" for="hazardous-materials-add-no-${length}">
                        <span class="circle-check-box mt-1"></span>
                        No
                    </label>
                </div>
            </div>
            <div class="hazardous-matetials-container mt-3"></div>
        </div>
    </div>
    <button type="button" class="button-secondary d-none truck-load-remove-additional-item float-end my-3">Remove Additional Item</button>
    </div>`
    $(".truck-load-add-remove-addition-container").append(html);
    $(".add-container").last().hide().show("slow")
    $(".truck-load-remove-additional-item").removeClass("d-none").show("slow")
});

// remove additional item (shipment and quote)
$(document).on("click", ".truck-load-remove-additional-item", function(){
    $(this).parent().hide("slow")
    $(this).parent().delay(1000, function(){
        $(this).remove();
    });
})

$(".button-add-reference-number").on("click", function(){
    const html = `<div class="reference-number-tag p-3 clearfix">
    <div class="row">
        <div class="col-6">
            <div class="group-input">
                <label for="">REFERENCE TYPE</label>
                <select class="form-select reference-type-select" name="" id="">
                    <option value="" selected="selected">Select</option>
                    <option value="BOL Number">BOL Number</option>
                    <option value="Cons Ref">Cons Ref</option>
                    <option value="CustInvRef">CustInvRef</option>
                    <option value="Delivery Appt Number">Delivery Appt Number</option>
                    <option value="GL Code">GL Code</option>
                    <option value="Job Name">Job Name</option>
                    <option value="Job Number">Job Number</option>
                    <option value="Manifest Nbr">Manifest Nbr</option>
                    <option value="Order Number">Order Number</option>
                    <option value="Pickup Appt Nbr">Pickup Appt Nbr</option>
                    <option value="Pickup Number">Pickup Number</option>
                    <option value="PO Number">PO Number</option>
                    <option value="Pro Number">Pro Number</option>
                    <option value="Product Code">Product Code</option>
                    <option value="Project Number">Project Number</option>
                    <option value="Release Nbr">Release Nbr</option>
                    <option value="Sales Order">Sales Order</option>
                    <option value="Ship Ref">Ship Ref</option>
                    <option value="WorkOrderNbr">WorkOrderNbr</option>
                    <option value="Carrier Quote Number">Carrier Quote Number</option>
                    <option value="RMA Number">RMA Number</option>
                    <option value="Buyer">Buyer</option>
                    <option value="Service Level">Service Level</option>
                    <option value="QuoteNumber">QuoteNumber</option>
                </select>
            </div>
        </div>
        <div class="col-6">
            <div class="group-input">
                <label for="">REFERENCE NUMBER</label>
                <input type="text" class="form-control reference-number" disabled>
            </div>
        </div>
    </div>
    <button type="button" class="button-secondary float-end button-remove-reference-number">Remove Reference Number</button>
</div>`
    $(".reference-number-container").append(html)
    $(".reference-number-tag").last().hide().show("slow")
})
$(document).on("click", ".button-remove-reference-number", function(){
    $(this).parent().hide("slow")
    $(this).parent().delay(1000, function(){
        $(this).remove();
    });
    // $(this).parent().remove();
})
$(document).on("change",".reference-type-select", function(){
    if($(this).val() != ""){
        $(".reference-number").removeAttr("disabled")
        $(".reference-number").prop("required", true);
        $(".reference-number").addClass("required");
    }
    else{
        $(".reference-number").prop("disabled", true)
        $(".reference-number").prop("required", false);
        $(".reference-number").removeClass("required");
    }
})
$(document).on("click", ".view-hazmat-details", function(){
    $(this).next().removeClass("d-none");
})
$(document).on("click",".bi-x-lg", function(){
    $(this).closest(".hazmat-info-container").addClass("d-none")
})

$(document).ready(function(){
    $(".group-circle-step:not(:first)").each(function(index){
        $(this).css("opacity", ".5");
        $(this).find(".arrow").addClass("d-none")
    })
})

$(document).on("click",".button-save-and-contrinue", function(e){
    var data = $(".shipment-truck-load-step").not(".d-none").attr("data-infor-curent")
    var ele =  $(`[data-infor-curent=${data}]`);
    if(validateInputs(ele)){
        var nextEle = $(`[data-infor-curent=${+data + 1}]`);
        if(+data + 1 === 3){
            $(this).children().text("Submit");
            $(this).attr("type", 'submit');
            e.preventDefault();
            $(ele).addClass("d-none");
            $(nextEle).removeClass("d-none");
            $(`.group-circle-step-${+data + 1}`).css("opacity", "1");
            removeClassArrow(+data + 1)
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
        else if(+data === 3){
            return;
        }
        else{
            $(".button-back").prop("disabled", false)
            $(ele).addClass("d-none");
            $(nextEle).removeClass("d-none");
            $(`.group-circle-step-${+data+1}`).css("opacity", "1");
            removeClassArrow(+data + 1)
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
    }
    else{
        Swal.fire({
            icon: 'warning',
            title: 'Warning!',
            text: 'Please fill all fields outlined in orange are required to go next step',
          })
    }
})

$(document).on("click",".button-back", function(){
    var data = $(".shipment-truck-load-step").not(".d-none").attr("data-infor-curent")
    var ele =  $(`[data-infor-curent=${data}]`);
    var preEle = $(`[data-infor-curent=${+data - 1}]`);
    $('.button-save-and-contrinue').attr("type", 'button');
    $('.button-save-and-contrinue').children().text('Next')
    if(+data - 1 === 1){
        $(".button-back").prop("disabled", true)
        $(ele).addClass("d-none");
        $(preEle).removeClass("d-none");
        $(`.group-circle-step-${+data}`).css("opacity", ".5");
        removeClassArrow(+data -1);
        window.scrollTo({top: 0, behavior: 'smooth'});
    }
    else if(+data === 1){
        return;
    }
    else{
        $(`.group-circle-step-${+data}`).css("opacity", "0.5");
        removeClassArrow(+data -1 )
        $(ele).addClass("d-none");
        $(preEle).removeClass("d-none");
        window.scrollTo({top: 0, behavior: 'smooth'});
    }
})

function removeClassArrow(index){
    $(".group-circle-step").each(function(){
        $(this).find(".arrow").addClass("d-none")
    })
    $(`.group-circle-step-${index}`).find(".arrow").removeClass("d-none");
}
function validateInputs(ele){
    var inputs = ele.find("input, select");
    var  a = 0;
    var fisrtElement;
    $.each(inputs, function(){
        if($(this).attr('required')){
            if ($(this).not('input[type=radio], input[type=checkbox]').length > 0) {
                if (!$(this).val()) {
                    if (!fisrtElement) fisrtElement = $(this);
                    a+= 1;
                }
            }
        }
    })

    if(a >= 1){
        fisrtElement.focus();
        window.scrollTo({top: $(fisrtElement).position().top - 300, behavior: 'smooth'});
        return false
    }
    else return true;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
///// Get quote LTL
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(".button-add-freight-des-get-quote").on('click', function(){
    const html = `<div class="row py-2 g-0 freight-description-item-get-quote g-2 border-bottom">
    <div class="col-6 ps-1">
        <div class="row align-items-center">
            <div class="col-10">
                <div class="group-input">
                    <input class="form-control required" type="text" required placeholder="Select or search for a class or saved item...">
                    <ul class="list-data-shipping search-for-class d-none">
                        <li>khanhtien</li>
                        <li>khanhtien2</li>
                        <li>khanhtien3</li>
                        <li>khanhtien4</li>
                        <li>khanhtien5</li>
                        <li>khanhtien6</li>
                    </ul>
                </div>
            </div>
            <div class="col-2">
                <a  href="https://worldcraftlogistics.com/pages/tool-density">Estimate class</a>
            </div>
            <div class="col-12">
                <div class="group-input">
                    <input class="form-control required" type="number" placeholder="Enter a weight" required>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="row g-1">
            <div class="col-6">
                <div class="group-input">
                    <input class="form-control " type="text" placeholder="Enter a quantity" >
                </div>
            </div>
            <div class="col-6">
                <select class="form-select" name="" id="">
                    <option value="Skids">Skids</option>
                    <option value="Bags">Bags</option>
                    <option value="Bundles">Bundles</option>
                    <option value="Boxes">Boxes</option>
                    <option value="Cabinets">Cabinets</option>
                    <option value="Cans">Cans</option>
                    <option value="Cases">Cases</option>
                    <option value="Crates">Crates</option>
                    <option value="Cartons">Cartons</option>
                    <option value="Cylinders">Cylinders</option>
                    <option value="Drums">Drums</option>
                    <option value="Pails">Pails</option>
                    <option value="Pieces">Pieces</option>
                    <option value="Pallets">Pallets</option>
                    <option value="Flat Racks">Flat Racks</option>
                    <option value="Reels">Reels</option>
                    <option value="Rolls">Rolls</option>
                    <option value="Slip Sheets">Slip Sheets</option>
                    <option value="Stacks">Stacks</option>
                    <option value="Totes">Totes</option>
                </select>
            </div>
            <div class="col-3">
                <div class="group-input">
                    <input class="form-control " type="text" placeholder="Length" >
                </div>
            </div>
            <div class="col-3">
                <div class="group-input">
                    <input class="form-control " type="text" placeholder="Width" >
                </div>
            </div>
            <div class="col-3">
                <div class="group-input">
                    <input class="form-control " type="text" placeholder="Height" >
                </div>
            </div>
            <div class="col-3">
                <select class="form-select" name="" id="">
                    <option value="IN">IN</option>
                    <option value="FT">FT</option>
                    <option value="CM">CM</option>
                    <option value="M">M</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-12"><button type="button" class="button-primary float-end button-remove-freight-des-get-quote">Remove Additional Item</button></div>
</div>`
    $(".container-freight-description-get-quote").append(html);
    $(".freight-description-item-get-quote").last().hide().show("slow");
})
$(document).on("click", ".button-remove-freight-des-get-quote", function(){
    $(this).parent().parent().hide("slow");
    $(this).parent().parent().delay(500, function(){
        $(this).remove();
    })
});
$(".btn-submit-get-quote-ltl").on("click", function(e){
    if(!validateInputs($('form[name=get-quote-LTL]'))){
        e.preventDefault();
        Swal.fire({
            icon: 'warning',
            title: 'Warning!',
            text: 'Please fill all fields outlined in orange are required to go next step',
          })
    }
    else{

    }
})

$(document).on("change", '.input-check-hazardous-materials', function(){
    const html = `<div class="row g-2">
    <!--UN NUMBER-->
    <div class="col-6">
        <div class="group-input">
            <label for="">UN NUMBER</label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
    <!--CCN NUMBER-->
    <div class="col-6">
        <div class="group-input">
            <label for="">CCN NUMBER<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control" value="CNN#">
        </div>
    </div>
    <!--HAZMAT PACKAGING GROUP-->
    <div class="col-6">
        <div class="group-input">
            <label for="">HAZMAT PACKAGING GROUP<span style="color: #ffce56;">*</span></label>
            <select class="form-select required" name="" id="" required>
                <option value="">Select Group</option>
                <option value="I">I</option>
                <option value="II">II</option>
                <option value="III">III</option>
                <option value="N/A">N/A</option>
            </select>
        </div>
    </div>
    <!---HAZMAT CLASS-->
    <div class="col-6">
        <div class="group-input">
            <label for="">HAZMAT CLASS<span style="color: #ffce56;">*</span></label>
            <select class="form-select required" required>
                <option value="">Select Hazmat class...</option>
                <option value="1">1</option>
                <option value="1.1">1.1</option>
                <option value="1.2">1.2</option>
                <option value="1.3">1.3</option>
                <option value="1.4">1.4</option>
                <option value="1.4G">1.4G</option>
                <option value="1.4S">1.4S</option>
                <option value="1.5">1.5</option>
                <option value="1.6">1.6</option>
                <option value="2">2</option>
                <option value="2.1">2.1</option>
                <option value="2.2">2.2</option>
                <option value="2.3">2.3</option>
                <option value="3">3</option>
                <option value="3(6.1)">3(6.1)</option>
                <option value="3(6.1)(8)">3(6.1)(8)</option>
                <option value="3(8)">3(8)</option>
                <option value="4">4</option>
                <option value="4.1">4.1</option>
                <option value="4.2">4.2</option>
                <option value="4.3">4.3</option>
                <option value="5">5</option>
                <option value="5.1">5.1</option>
                <option value="5.1(8)">5.1(8)</option>
                <option value="5.1(8)(6.1)">5.1(8)(6.1)</option>
                <option value="5.2">5.2</option>
                <option value="5.2(8)">5.2(8)</option>
                <option value="6">6</option>
                <option value="6.1">6.1</option>
                <option value="6.1(3)(8)">6.1(3)(8)</option>
                <option value="6.1(8)">6.1(8)</option>
                <option value="6.2">6.2</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="8(3)">8(3)</option>
                <option value="8(5.1)">8(5.1)</option>
                <option value="8(6.1)">8(6.1)</option>
                <option value="9">9</option>
            </select>
        </div>
    </div>
    <!--EMERGENCY RESPONSE CONTACT NAME *-->
    <div class="col-6">
        <div class="group-input">
            <label for="">EMERGENCY RESPONSE CONTACT NAME<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
    <div class="col-6">
        <div class="group-input">
            <label for="">EMERGENCY RESPONSE PHONE NUMBER<span style="color: #ffce56;">*</span></label>
            <input type="text" class="form-control required" required>
        </div>
    </div>
</div>
`
    if($(this).val() == "True"){
        $(this).closest(".expand-yes").find(".hazardous-matetials-container").append(html).hide().show("slow");
    }
    else if($(this).val() == "False"){
        $(this).closest(".expand-yes").find(".hazardous-matetials-container").hide("slow");
        $(this).closest(".expand-yes").find(".hazardous-matetials-container").empty();
    }
})

$(document).on("click",".get-quote-truck-load-add-additional-item", function(){
    var length =  $(".truck-load-add-remove-addition-container").find(".add-container").length;
    const html = `<div class="add-container pt-3 ps-3 clearfix">
    <div class="row">
        <div class="col-3">
            <div class="group-input">
                <label for="">QUANTITY <span style="color: #ffce56;">*</span></label>
                <input class="form-control" type="number" name="" id="">
            </div>
        </div>
        <div class="col-3">
            <label for=""></label>
            <select class="form-select" name="" id="">
                <option value="Bags">Bags</option>
                <option value="Bundles">Bundles</option>
                <option value="Boxes">Boxes</option>
                <option value="Cabinets">Cabinets</option>
                <option value="Cans">Cans</option>
                <option value="Cases">Cases</option>
                <option value="Crates">Crates</option>
                <option value="Cartons">Cartons</option>
                <option value="Cylinders">Cylinders</option>
                <option value="Drums">Drums</option>
                <option value="Pails">Pails</option>
                <option value="Pieces">Pieces</option>
                <option value="Pallets">Pallets</option>
                <option value="Flat Racks">Flat Racks</option>
                <option value="Reels">Reels</option>
                <option value="Rolls">Rolls</option>
                <option value="Skids">Skids</option>
                <option value="Slip Sheets">Slip Sheets</option>
                <option value="Stacks">Stacks</option>
                <option value="Totes">Totes</option>
            </select>
        </div>
        <div class="col-3">
            <div class="group-input">
                <label for="">WEIGHT<span style="color: #ffce56;">*</span></label>
                <input type="number" class="form-control required" required>
            </div>
        </div>
        <div class="col-3">
            <div class="group-input">
                <label for=""></label>
                <select name="" id="" class="form-select">
                    <option value="LBS">LBS</option>
                    <option value="KILOS">KILOS</option>
                </select>
            </div>
        </div>
        <div class="col-12 mt-3">
            <div class="group-input">
                <label for="">ITEM DESCRIPTION <span style="color: #ffce56;">*</span></label>
                <input type="text" class="form-control required" required>
            </div>
        </div>
        <div class="col-12 mt-3 expand-yes">
            <div class="group-input">
                <label for="" class="d-block mb-3">HAZARDOUS MATERIALS<span style="color: #ffce56;">*</span></label>
                <div class="check-box-radio-group">
                    <input class="check-box-input d-none input-check-hazardous-materials" type="radio" id="hazardous-materials-add-yes-${length}" value="True" name="hazardous-materials-get-quote-${length}">
                    <label class="check-box-label" for="hazardous-materials-add-yes-${length}">
                        <span class="circle-check-box mt-1"></span>
                        Yes
                    </label>
                </div>

                <div class="check-box-radio-group">
                    <input class="check-box-input d-none input-check-hazardous-materials" type="radio" id="hazardous-materials-add-no-${length}" value="False" name="hazardous-materials-get-quote-${length}">
                    <label class="check-box-label" for="hazardous-materials-add-no-${length}">
                        <span class="circle-check-box mt-1"></span>
                        No
                    </label>
                </div>
            </div>
            <div class="hazardous-matetials-container mt-3"></div>
        </div>

    </div>
    <button type="button" class="button-secondary d-none truck-load-remove-additional-item float-end my-3">Remove Additional Item</button>
</div>`
    $(".truck-load-add-remove-addition-container").append(html);
    $(".add-container").last().hide().show("slow")
    $(".truck-load-remove-additional-item").removeClass("d-none").show("slow")
});

// button click on save and next - back get quote
$(document).on("click",".button-save-and-contrinue-get-quote", function(e){
    var data = $(".shipment-truck-load-step").not(".d-none").attr("data-infor-curent")
    var ele =  $(`[data-infor-curent=${data}]`);
    if(validateInputs(ele)){
        var nextEle = $(`[data-infor-curent=${+data + 1}]`);
        if(+data + 1 === 2){
            $(".button-back-get-quote").prop("disabled", false)
            $(this).children().text("Submit");
            $(this).attr("type", 'submit');
            e.preventDefault();
            $(ele).addClass("d-none");
            $(nextEle).removeClass("d-none");
            $(`.group-circle-step-${+data + 1}`).css("opacity", "1");
            removeClassArrow(+data + 1)
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
        else if(+data === 2){
            return;
        }
        else{
            $(".button-back-get-quote").prop("disabled", false)
            $(ele).addClass("d-none");
            $(nextEle).removeClass("d-none");
            $(`.group-circle-step-${+data+1}`).css("opacity", "1");
            removeClassArrow(+data + 1)
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
    }
    else{
        Swal.fire({
            icon: 'warning',
            title: 'Warning!',
            text: 'Please fill all fields outlined in orange are required to go next step',
          })
    }
})

$(document).on("click",".button-back-get-quote", function(){
    var data = $(".shipment-truck-load-step").not(".d-none").attr("data-infor-curent")
    var ele =  $(`[data-infor-curent=${data}]`);
    var preEle = $(`[data-infor-curent=${+data - 1}]`);
    $('.button-save-and-contrinue-get-quote').attr("type", 'button');
    $('.button-save-and-contrinue-get-quote').children().text('Next')
    if(+data - 1 === 1){
        $(".button-back-get-quote").prop("disabled", true)
        $(ele).addClass("d-none");
        $(preEle).removeClass("d-none");
        $(`.group-circle-step-${+data}`).css("opacity", ".5");
        removeClassArrow(+data -1);
        window.scrollTo({top: 0, behavior: 'smooth'});
    }
    else if(+data === 1){
        return;
    }
    else{
        $(`.group-circle-step-${+data}`).css("opacity", "0.5");
        removeClassArrow(+data -1 )
        $(ele).addClass("d-none");
        $(preEle).removeClass("d-none");
        window.scrollTo({top: 0, behavior: 'smooth'});
    }
})

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Account Manager
var checkAcc = true;
$(".button-update-infor").on('click', function(){
    if(checkAcc){
        $('.update-infor').css({
            'width': '40rem',
            'overflow': 'visible'
        });
        $('.main-infor').css('width', '30rem')
        $(this).text("Information")
        checkAcc = !checkAcc;
    }
    else if(!checkAcc){
        $('.update-infor').css({
            'width': '0',
            'overflow': 'hidden'
        });
        $('.main-infor').css('width', '50rem')
        checkAcc = !checkAcc;
        $(this).text("Update infor")
    }
})
$(".button-submit-update-account").on('click', function(e){
    e.preventDefault()
    var ele = $('form[name="account-update"]')

})

$(".btn-submit-form-claim-submit").on("click", function(e){
    if(!validateInputs($("form[name=form-claim-submit]"))){
        e.preventDefault();
        Swal.fire({
            icon: 'warning',
            title: 'Warning!',
            text: 'Please fill all fields outlined in orange are required to go next step',
          })
    }
})
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//handle file up load ClaimSubmit
$("#file-claim").on("change", function(e){
    var checkFile;
    upload(e.target.files)
})

$('.container-drop-file').on(
    'dragover',
    function(e) {
        e.preventDefault();
        e.stopPropagation();
    }
)
$('.container-drop-file').on(
    'dragenter',
    function(e) {
        e.preventDefault();
        e.stopPropagation();
    }
)

$('.container-drop-file').on(
    'drop',
    function(e){
        if(e.originalEvent.dataTransfer){
            if(e.originalEvent.dataTransfer.files.length) {
                var check = true;
                e.preventDefault();
                e.stopPropagation();
                upload(e.originalEvent.dataTransfer.files);
                var inputFile = $("#file-claim")
                $.each(e.originalEvent.dataTransfer.files, function(index, item){
                    if(item.size > 10485760){
                        check =false;
                        return false;
                    }
                })
                if(check){
                    inputFile.files = e.originalEvent.dataTransfer.files;
                }
                console.log(inputFile.files)
            }   
        }
    }
);
function upload (file){
    var fileOver10MB = [];
    if(file.length > 1){
        var name = [];
        $.each(file, function(index, item){
            name.push(item.name)
            if(item.size > 10485760){
                fileOver10MB.push(item);
            }
        })
        
        if(fileOver10MB.length >= 1){
            $("#file-claim").val("");
            Swal.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Individual file size must not exceed 10MB!',
              })
            $(".file-claim-label span").text("No file currently uploaded")
        }
        else{
            $(".file-claim-label span").text(name.join(";"))
        }
    }
    else{
        //10485760
        if(file[0].size > 10485760){
            Swal.fire({
                icon: 'warning',
                title: 'Warning!',
                text: 'Individual file size must not exceed 10MB!',
              })
              $(".file-claim-label span").text("No file currently uploaded")
        }
       else{
            $(".file-claim-label span").text(file.item(0)?.name)
       }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$(document).on("click", ".button-save-and-contrinue", function (e) {
    var data = $(".shipment-truck-load-step").not(".d-none").attr("data-infor-curent")
    var ele = $(`[data-infor-curent=${data}]`);
    if (validateInputs(ele)) {
        var nextEle = $(`[data-infor-curent=${+data + 1}]`);
        if (+data + 1 === 3) {
            $(this).children().text("Submit");
            $(this).attr("type", 'submit');
            e.preventDefault();
            $(ele).addClass("d-none");
            $(nextEle).removeClass("d-none");
            $(`.group-circle-step-${+data + 1}`).css("opacity", "1");
            removeClassArrow(+data + 1)
            window.scrollTo({ top: 0, behavior: 'smooth' });


            $(".pickup-location-name").val($("#Origin_Infor_Name").val());
            $(".pickup-location-address").val($("#Origin_Infor_Address1").val());
            $(".pickup-location-comments").val($("#Origin_Infor_Comment").val());
            $(".pickup-contact-inf-name").val($("#Pickup_Infor_Name").val());
            $(".pickup-contact-inf-phone").val($("#Pickup_Infor_Phone").val());
            $(".pickup-contact-inf-fax").val($("#Pickup_Infor_FaxNumber").val());
            $(".pickup-contact-inf-email").val($("#Pickup_Infor_Email").val());
            $(".pickup-date-pickup").val($(".Pickup_Date").val());

            $(".dropoff-location-name").val($("#Destination_Location_Name").val());
            $(".dropoff-location-address").val($("#Destination_Location_Address1").val());
            $(".dropoff-location-comments").val($("#Destination_Location_Comment").val());
            $(".dropoff-contact-inf-name").val($("#Drop_Infor_Name").val());
            $(".dropoff-contact-inf-phone").val($("#Drop_Infor_Phone").val());
            $(".dropoff-contact-inf-fax").val($("#Drop_Infor_FaxNumber").val());
            $(".dropoff-contact-inf-email").val($("#Drop_Infor_Email").val());
            $(".dropoff-date-pickup").val($("#Drop_Date").val());

            if ($("input[name=hazardous-materials]:checked").val() === "True") {
                var html = `<tr>
                                            <td>${$(".freight-description-main").find(".freight-description-QTY").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-QTY-Unit").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-weight").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-description").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-class").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-NMFC").val()}</td>
                                            <td>${$("#Shipment_Value")}</td>
                                            <td>True</td>
                                            <td class="position-relative">
                                                <span class="view-hazmat-details">VIEW HAZMAT DETAILS</span>
                                                <div class="hazmat-info-container position-absolute d-none">
                                                    <div class="row">
                                                        <div class="col-8"><h6>HAZMAT INFORMATION</h6></div>
                                                        <div class="col-4"><i class="bi bi-x-lg float-end pe-3" style="cursor: pointer;"></i></div>

                                                        <div class="col-8"><p><strong>Proper Shipment Name</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-shipment-name">${"proper shipment"}</p></div>

                                                        <div class="col-8"><p><strong>UN Number</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-un-munber">${$(".freight-description-main").find(".Haz_UnNumber").val()}</p></div>

                                                        <div class="col-8"><p><strong>CCN Number</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-ccn-munber">${$(".freight-description-main").find(".Haz_CCNNumber").val()}</p></div>

                                                        <div class="col-8"><p><strong>Hazmat Package Group</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-package-group">${$(".freight-description-main").find(".Haz_Packaging_Group").val()}</p></div>

                                                        <div class="col-8"><p><strong>Hazmat Class</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-class">${$(".freight-description-main").find(".Haz_Class").val()}</p></div>

                                                        <div class="col-8"><p><strong>Emergency Response Contact Name</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-contact-name">${$(".freight-description-main").find(".Haz_EmerContactName").val()}</p></div>

                                                        <div class="col-8"><p><strong>Emergency Response Phone Number</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-phone-number">${$(".freight-description-main").find(".Haz_EmerPhoneNumber").val()}</p></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>`
                $("#freight-description-data-table").append(html);
            }
            else {
                var html = `<tr>
                                            <td>${$(".freight-description-main").find(".freight-description-QTY").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-QTY-Unit").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-weight").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-description").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-class").val()}</td>
                                            <td>${$(".freight-description-main").find(".freight-description-NMFC").val()}</td>
                                            <td>${$("#Shipment_Value")}</td>
                                            <td>False</td>
                                        </tr>`
                $("#freight-description-data-table").append(html);
            }
            $(".truck-load-add-remove-addition-container > .add-container").each(function (index) {
                if ($(this).find(`input[name=hazardous-materials-${index}]:checked`).val() === "True") {
                    var html = `<tr>
                                            <td>${$(this).find(".freight-description-QTY").val()}</td>
                                            <td>${$(this).find(".freight-description-QTY-Unit").val()}</td>
                                            <td>${$(this).find(".freight-description-weight").val()}</td>
                                            <td>${$(this).find(".freight-description-description").val()}</td>
                                            <td>${$(this).find(".freight-description-class").val()}</td>
                                            <td>${$(this).find(".freight-description-NMFC").val()}</td>
                                            <td>${$("#Shipment_Value")}</td>
                                            <td>True</td>
                                            <td class="position-relative">
                                                <span class="view-hazmat-details">VIEW HAZMAT DETAILS</span>
                                                <div class="hazmat-info-container position-absolute d-none">
                                                    <div class="row">
                                                        <div class="col-8"><h6>HAZMAT INFORMATION</h6></div>
                                                        <div class="col-4"><i class="bi bi-x-lg float-end pe-3" style="cursor: pointer;"></i></div>

                                                        <div class="col-8"><p><strong>Proper Shipment Name</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-shipment-name">${"proper shipment"}</p></div>

                                                        <div class="col-8"><p><strong>UN Number</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-un-munber">${$(this).find(".Haz_UnNumber").val()}</p></div>

                                                        <div class="col-8"><p><strong>CCN Number</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-ccn-munber">${$(this).find(".Haz_CCNNumber").val()}</p></div>

                                                        <div class="col-8"><p><strong>Hazmat Package Group</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-package-group">${$(this).find(".Haz_Packaging_Group").val()}</p></div>

                                                        <div class="col-8"><p><strong>Hazmat Class</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-class">${$(this).find(".Haz_Class").val()}</p></div>

                                                        <div class="col-8"><p><strong>Emergency Response Contact Name</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-contact-name">${$(this).find(".Haz_EmerContactName").val()}</p></div>

                                                        <div class="col-8"><p><strong>Emergency Response Phone Number</strong></p></div>
                                                        <div class="col-4"><p class="hazmat-inf-proper-phone-number">${$(this).find(".Haz_EmerPhoneNumber").val()}</p></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>`
                    $("#freight-description-data-table").append(html);
                }
                else {
                    var html = `<tr>
                                            <td>${$(this).find(".freight-description-QTY").val()}</td>
                                            <td>${$(this).find(".freight-description-QTY-Unit").val()}</td>
                                            <td>${$(this).find(".freight-description-weight").val()}</td>
                                            <td>${$(this).find(".freight-description-description").val()}</td>
                                            <td>${$(this).find(".freight-description-class").val()}</td>
                                            <td>${$(this).find(".freight-description-NMFC").val()}</td>
                                            <td>${$("#Shipment_Value")}</td>
                                            <td>False</td>
                                        </tr>`
                    $("#freight-description-data-table").append(html);
                }
        })
        }
        else if(+data === 3){
            return;
        }
        else{
            $(".button-back").prop("disabled", false)
            $(ele).addClass("d-none");
            $(nextEle).removeClass("d-none");
            $(`.group-circle-step-${+data+1}`).css("opacity", "1");
            removeClassArrow(+data + 1)
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
    }
    else{
        Swal.fire({
            icon: 'warning',
            title: 'Warning!',
            text: 'Please fill all fields outlined in orange are required to go next step',
          })
    }
})
